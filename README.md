# Flask-SQLAlchemy-Serverless

## Use Cases

* Reuse your SQLite database in serverless applications and reduce prototyping costs.
* Synchronize your local SQLite database across multiple instances.

## How it Works
* Different storage vendors can be used and easily added.
* Commits result in a direct upload of your database.
* To reduce API calls when reading a database, a cache_duration can be configured.

## Limitations  
Multiple write accesses in parallel can lead to data loss. Ideal for read-only applications.
Because the database is transferred as a whole file, large databases can cause latency problems.

## Available Integrations
- AWS S3
- [Comming Soon] All storage supported by [Apache LibCloud](https://libcloud.apache.org/)

## Example

### Install
```bash
pip install .

```
### Usage
````python
import sqlalchemy_cloudsqlite
SQLALCHEMY_DATABASE_URI = "cloudsqlite:///<bucker-name>/quickstart.sqlite"
...
engine = create_engine(SQLALCHEMY_DATABASE_URI)
````

Manually register the dialect:

```python
from flask_sqlalchemy import SQLAlchemy

# this is the important change, it imports sqlalchemy-s3sqlite at runtime
from sqlalchemy.dialects import registry
registry.register("cloudsqlite", "sqlalchemy_cloudsqlite.dialect", "CloudSQLiteDialect")
```


#### Configuration
```
SLS_SQLITE_BUCKET='sqlite-storage-bucket'
SLS_CACHE_DURATION=0

import json
os.environ['config'] = json.dumps(
    {
        'cache_duration': 60,
        'storage': {
            'S3': {'bucket_name': '<BUCKET_NAME>'}
        }
    }
)
```
and provide your credentials for S3 access via environment variables or a policy. 

## About
This project is based on the following research:
- [Original Code Repo](https://github.com/NJannasch/SQLAlchemy-CloudSQLite.git)
- [S3SQLite - A Serverless Relational Database](https://blog.zappa.io/posts/s3sqlite-a-serverless-relational-database)
- [zappa-django-utils](https://github.com/Miserlou/zappa-django-utils)
