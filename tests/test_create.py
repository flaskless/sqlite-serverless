


def test_can_create():
    import sqlalchemy
    from sqlalchemy import create_engine

    engine = create_engine('cloudsqlite:////saasless-cloud-database/test/test1.db', echo=True)

    from sqlalchemy.orm import sessionmaker
    Session = sessionmaker(bind=engine)
    session = Session()

    from sqlalchemy.ext.declarative import declarative_base
    Base = declarative_base()
    from sqlalchemy import Column, Integer, String
    class SimpleTable(Base):
        __tablename__ = 'simple_table'

        id = Column(Integer, primary_key=True)
        name = Column(String)

        def __repr__(self):
            return f'Simple Entry {self.name}'

    Base.metadata.create_all(engine)